﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AsianFrog.Data.Migrations
{
    public partial class AddedPondToFrog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PondID",
                table: "Frogs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Frogs_PondID",
                table: "Frogs",
                column: "PondID");

            migrationBuilder.AddForeignKey(
                name: "FK_Frogs_Ponds_PondID",
                table: "Frogs",
                column: "PondID",
                principalTable: "Ponds",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Frogs_Ponds_PondID",
                table: "Frogs");

            migrationBuilder.DropIndex(
                name: "IX_Frogs_PondID",
                table: "Frogs");

            migrationBuilder.DropColumn(
                name: "PondID",
                table: "Frogs");
        }
    }
}
