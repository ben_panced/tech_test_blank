﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AsianFrog.Data.Migrations
{
    public partial class InitiliazePonds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
            @"
                INSERT INTO PONDS VALUES ('Tasik Titiwangsa', 'KL')
            ");

            migrationBuilder.Sql(
            @"
                INSERT INTO PONDS VALUES ('Tasik Shah Alam', 'Selangor')
            ");

            migrationBuilder.Sql(
            @"
                INSERT INTO PONDS VALUES ('Tasik Kenyir', 'Terengganu')
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
            @"
                TRUNCATE TABLE PONDS
            ");
        }
    }
}
