﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsianFrog.Models
{
    public class CreateFrogViewModel
    {
        public CreateFrogViewModel()
        {
            Ponds = new List<Pond>();   
        }

        public ICollection<Pond> Ponds;
        public Frog Frog;
    }
}
