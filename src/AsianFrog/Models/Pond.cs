﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AsianFrog.Models
{
    public class Pond
    {
        public int ID { get; set; }

        [Required]
        [Display(Name="Pond Name")]
        public string Name { get; set; }

        [Required]
        public string Location { get; set; }

        public ICollection<Frog> Frog { get; set; }
    }
}
