﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsianFrog.Models
{
    public class FrogViewModel
    {
        public FrogViewModel()
        {
            Frogs = new List<Frog>();
        }

        public ICollection<Frog> Frogs { get; set; }
        public Frog Frog { get; set; }
        public int FrogID1 { get; set; }
        public int FrogID2 { get; set; }

    }
}
