﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace AsianFrog.Models
{
    public class Frog
    {

        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:d-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:d-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DeathDate { get; set; }

        [ForeignKey("Pond")]
        public int PondID { get; set; }
        public Pond Pond { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    };
}
