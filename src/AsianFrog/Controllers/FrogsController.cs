﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AsianFrog.Data;
using AsianFrog.Models;
using Microsoft.AspNetCore.Server.Kestrel.Transport.Libuv.Internal.Networking;
using SQLitePCL;
using Microsoft.AspNetCore.Authorization;

namespace AsianFrog.Controllers
{
    [Authorize]
    public class FrogsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FrogsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Frogs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Frogs.ToListAsync());
        }

        // GET: Frogs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var frog = await _context.Frogs
                .SingleOrDefaultAsync(m => m.ID == id);
            if (frog == null)
            {
                return NotFound();
            }

            return View(frog);
        }

        // GET: Frogs/Create
        public IActionResult Create()
        {
            CreateFrogViewModel model = new CreateFrogViewModel();
            foreach (Pond pond in _context.Ponds)
            {
                model.Ponds.Add(pond);
            }

            return View(model);
        }

        // POST: Frogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Gender,BirthDate,DeathDate,PondID")] Frog frog)
        {
            CreateFrogViewModel model = new CreateFrogViewModel();
            if (ModelState.IsValid)
            {
                _context.Add(frog);
                model.Frog = frog;
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Frogs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var frog = await _context.Frogs.SingleOrDefaultAsync(m => m.ID == id);
            if (frog == null)
            {
                return NotFound();
            }
            return View(frog);
        }

        // POST: Frogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,BirthDate,DeathDate")] Frog frog)
        {
            if (id != frog.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(frog);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FrogExists(frog.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(frog);
        }

        // GET: Frogs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var frog = await _context.Frogs
                .SingleOrDefaultAsync(m => m.ID == id);
            if (frog == null)
            {
                return NotFound();
            }

            return View(frog);
        }

        // POST: Frogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var frog = await _context.Frogs.SingleOrDefaultAsync(m => m.ID == id);
            _context.Frogs.Remove(frog);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FrogExists(int id)
        {
            return _context.Frogs.Any(e => e.ID == id);
        }

        public async Task<IActionResult> Mate(int id)
        {
            var model = new FrogViewModel();
            var currentFrog = _context.Frogs.Where(b => b.ID == id).Include(b => b.Pond).FirstOrDefault();
            
            model.Frog = currentFrog;
            model.FrogID1 = id;

            foreach (Frog frog in _context.Frogs)
            {
                if (currentFrog.Pond.ID == frog.Pond.ID && currentFrog.Gender != frog.Gender && currentFrog.ID != frog.ID)
                {
                    model.Frogs.Add(frog);
                }
            }

            return View(model);
        }

        // POST: Frogs/Mate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Mate([Bind("FrogID1, FrogID2")] FrogViewModel fvm)
        {
            var frog1 = new Frog();
            var frog2 = new Frog();
            var frog3 = new Frog();
            Random rand = new Random();

            frog1 = _context.Frogs.Where(b => b.ID == fvm.FrogID1).Include(b => b.Pond).FirstOrDefault();
            frog2 = _context.Frogs.Where(b => b.ID == fvm.FrogID2).Include(b => b.Pond).FirstOrDefault();

            if (rand.Next(0, 2) == 0)
                frog3.Gender = Gender.Male;
            else
                frog3.Gender = Gender.Female;

            if (frog1.Gender == Gender.Male)
                frog3.Name = frog1.Name + " " + frog2.Name;
            else
                frog3.Name = frog2.Name + " " + frog1.Name;
            frog3.BirthDate = DateTime.Now;
            frog3.PondID = frog1.PondID;

            _context.Add(frog3);

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
