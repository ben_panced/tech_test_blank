<!---
Add your comments / notes and thoughts to this doc

Any special instructions to running your code?

Instructions:

1. Git Clone this repository.
2. Open in visual studio.
3. Restore nuget packages.
4. In package manager console, run Update-Database to apply all migrations.
5. The solution can be built and tested.
-->
